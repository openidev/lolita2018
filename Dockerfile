FROM maven:3.5.4-jdk-11-slim AS builder
RUN java -version
RUN mvn --version

COPY . /usr/src/myapp/
WORKDIR /usr/src/myapp/
RUN mvn package

FROM maven:3.5.4-jdk-11-slim
WORKDIR /root/
COPY --from=builder /usr/src/myapp/target/lolita.jar .

EXPOSE 3000

ENV JAVA_OPTS -Xms256m -Xmx512m

ENTRYPOINT ["java", "-Xmx512m", "-jar", "./lolita.jar"]

CMD java -Xmx512m -jar lolita.jar

# docker build -f Dockerfile -t lolita:4 .