package org.bitbucket.lolita2018;

import org.bitbucket.lolita2018.exe.UserInfo;
import org.bitbucket.lolita2018.plugins.CacheProvider;
import org.bitbucket.lolita2018.plugins.DistributionResultsPlugins;
import org.bitbucket.lolita2018.plugins.HogwartsPlugin;
import org.bitbucket.lolita2018.utils.DurationResult;
import org.bitbucket.lolita2018.utils.Value;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.*;


public class HogwartsPluginTest {

  @Test
  void testDurationResult() {
    var now = LocalDateTime.now();

    var next = now.plusHours(10).plusMinutes(25).plusSeconds(30);
    var d = DurationResult.Build(now, next);
    Assert.assertThat(d.getHours(), equalTo(10L));
    Assert.assertThat(d.getMinutes(), equalTo(25));
    Assert.assertThat(d.toString(), equalTo("10H 25m"));


    var next1 = now.plusHours(25);
    var d1 = DurationResult.Build(now, next1);
    Assert.assertThat(d1.getHours(), equalTo(25L));
    Assert.assertThat(d1.getMinutes(), equalTo(0));
    Assert.assertThat(d1.toString(), equalTo("25H"));

    var next2 = now.plusMinutes(1);
    var d2 = DurationResult.Build(now, next2);
    Assert.assertThat(d2.getHours(), equalTo(0L));
    Assert.assertThat(d2.getMinutes(), equalTo(1));
    Assert.assertThat(d2.toString(), equalTo("1m"));

  }

  @Test
  void testRun() throws Exception {
    List<String> args = List.of();
    var provider = new CacheProvider(1, TimeUnit.SECONDS);
    var plugin = new HogwartsPlugin(provider);

    var user1 = new UserInfo(1, "a");
    var user2 = new UserInfo(2, "b");

    var d1 = plugin.run(args, user1).get().getResult();
    Assert.assertThat(d1, not(is(containsString("Next"))));

    var d2 = plugin.run(args, user2).get().getResult();
    Assert.assertThat(d2, not(is(containsString("Next"))));

    var d3 = plugin.run(args, user1).get().getResult();
    Assert.assertThat(d3, is(containsString("Next")));

    Thread.sleep(1000);

    var d5 = plugin.run(args, user1).get().getResult();
    Assert.assertThat(d5, not(is(containsString("Next"))));
  }

  @Test
  void testDistribution() throws Exception {
    var provider = new CacheProvider(10, TimeUnit.SECONDS);
    var plugin = new DistributionResultsPlugins(provider);

    var users = List.of(
        gen(1,"user1", "a"),
        gen(2, "user2", "b"),
        gen( 3, "user3", "c"),
        gen(4, "user4", "d"),
        gen(5, "user5", "a"),
        gen(6, "user6", "b")
    );

    users.forEach((u) -> provider.put(u.getUserId(), u));

    var results = plugin.run(List.of(), new UserInfo(1, "a")).get().getResult();

    for(var u: users) {
      Assert.assertThat(results, is(containsString(u.getUserName())));
      Assert.assertThat(results, is(containsString(u.getVoteResult())));
    }

    var xs = results.split("\n");

    Assert.assertEquals(xs.length, 4);
  }

  private Value gen(long uId, String name, String res) {
    return new Value(uId, name, res, LocalDateTime.now());
  }

}
