package org.bitbucket.lolita2018;

import org.bitbucket.lolita2018.utils.MapValue;
import org.bitbucket.lolita2018.utils.RandomUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class RandomUtilTests {

  private static Map<Integer, MapValue> map = new HashMap<>();

  private static ArrayList<String> xs = new ArrayList<>();

  private static String e1 = "a";
  private static String e2 = "b";
  private static String e3 = "c";

  private static String r1 = "r1";

  @BeforeEach
  private void init() {
    map.clear();
    xs.add(e1);
    xs.add(e2);
    xs.add(e3);
  }

  @Test
  void getRandomWhenMapIsEmpty() {
    var result = RandomUtil.getRandomElement(xs, map);
    assertTrue(xs.contains(result));
  }

  @Test
  void getRandomWhenMapContainElementsFromCollection() {
    push(e1);
    push(e2);
    var result = RandomUtil.getRandomElement(xs, map);
    assertEquals(e3, result);
  }

  @Test
  void getInOrderWhenAllElementsArePresentInMap() {
    push(e2);
    push(e3);
    push(e1);
    var result = RandomUtil.getRandomElement(xs, map);
    assertEquals(e2, result);
  }

  private static void push(String e) {
    map.putIfAbsent(e.hashCode(), MapValue.Create(e));
  }


}
