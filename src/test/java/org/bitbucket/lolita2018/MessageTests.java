package org.bitbucket.lolita2018;

import static org.bitbucket.lolita2018.exe.Message.from;
import com.pengrad.telegrambot.model.Chat;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class MessageTests {

  @Mock
  private Update update;

  @Mock
  private Message message;

  @Mock
  private Chat chat;

  @Mock
  private User user;

  private long chatId = 1L;
  private Integer messageId = Integer.valueOf(10);
  private int userId = 10;

  @BeforeEach
  void mockitify() {
    Mockito.when(update.message()).thenReturn(message);
    Mockito.when(message.chat()).thenReturn(chat);
    Mockito.when(chat.id()).thenReturn(chatId);
    Mockito.when(message.messageId()).thenReturn(messageId);
    Mockito.when(user.id()).thenReturn(userId);
    Mockito.when(message.from()).thenReturn(user);
  }

  @Test
  void parseValidCommandWithoutArguments() {
    Mockito.when(message.text()).thenReturn("! b");
    var result = from(update);
    assertEquals(result.getCommand(), "b");
    assertTrue(result.getArguments().isEmpty());
  }

  @Test
  void parseValidCommandWithArguments() {
    Mockito.when(message.text()).thenReturn("! b 123");
    var result = from(update);
    assertEquals(result.getCommand(), "b");
    var expected = new ArrayList<String>();
    expected.add("123");
    assertIterableEquals(result.getArguments(), expected);
  }

  @Test
  void parseNotValidCommand() {
    Mockito.when(message.text()).thenReturn("!b");
    var result = from(update);
    assertTrue(result.getCommand().isEmpty());
    assertTrue(result.getArguments().isEmpty());
  }

  @Test
  void parseAndTrimCommand() {
    Mockito.when(message.text()).thenReturn("!  b");
    var result = from(update);
    assertEquals("b", result.getCommand());
    assertTrue(result.getArguments().isEmpty());
  }

}
