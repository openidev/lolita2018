package org.bitbucket.lolita2018;

import java.util.ArrayList;
import java.util.List;

public class Configuration {
  private final String command;
  private final String token;
  private final long chatId;
  private final List<Long> chats;

  public Configuration(String command, String token,
                       long chatId,
                       List<Long> chats
                       ) {
    this.command = command;
    this.token = token;
    this.chatId = chatId;
    this.chats = chats;
  }

  public long getChatId() {
    return chatId;
  }

  public String getCommand() {
    return command;
  }

  public String getToken() {
    return token;
  }

  public List<Long> getChats() {
    return new ArrayList<>(chats);
  }

  public static class Fields {
    private static final String root = "telegram";
    public static final String fCommand =  root + ".command";
    public static final String fToken = root + ".token";
    public static final String fChatId = root + ".chatId";
    public static final String fChats = root + ".chats";
  }
}

