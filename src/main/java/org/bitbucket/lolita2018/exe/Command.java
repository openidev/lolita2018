package org.bitbucket.lolita2018.exe;

import org.bitbucket.lolita2018.plugins.Plugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class Command {

  protected final String command;
  protected final String helpInfo;
  protected final Plugin handler;
  protected final List<String> commands;

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  public Command(String command, List<String> aliases,
                 String helpInfo, Plugin handler) {
    this.command = command;
    this.helpInfo = buildHelp(command, helpInfo, aliases);
    this.handler = handler;

    logger.info("Construct '" + command + "' command");

    commands = new ArrayList<>();
    commands.add(command);
    commands.addAll(aliases);
  }

  public String help() {
    return helpInfo;
  }

  public boolean match(String value) {
    return commands.stream().anyMatch((x) -> x.equalsIgnoreCase(value));
  }

  public Optional<CompletableFuture<CommandResult>> handle(Message message) {
    var args = message.getArguments();
    String tmp = "no args";
    if (!args.isEmpty()) {
      tmp = String.join(", ", args);
    }
    logger.info("Run '" + command + "' command with: " + tmp);
    try {
      return Optional.of(handler.run(args, message.getUser()));
    } catch (Exception ex) {
      logger.warn("Error when run " + command + " -> " + ex.getMessage());
      return Optional.empty();
    }
  }

  private static String buildHelp(String command, String helpInfo, List<String> aliases) {
    var s = new StringBuilder();
    if (aliases.isEmpty()) {
      s.append(command)
          .append(" --- ")
          .append(helpInfo);
    } else {
      s.append(command).append(" ")
          .append("[").append(String.join(", ", aliases)).append("]")
          .append(" --- ")
          .append(helpInfo);
    }

    return s.toString();
  }

}
