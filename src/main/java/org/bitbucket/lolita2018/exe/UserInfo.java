package org.bitbucket.lolita2018.exe;

public class UserInfo {
  private final long userId;
  private final String name;

  public UserInfo(long userId, String name) {
    this.userId = userId;
    this.name = name;
  }

  public long getUserId() {
    return userId;
  }

  public String getName() {
    return name;
  }

}
