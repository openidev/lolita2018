package org.bitbucket.lolita2018.exe;

import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.request.SendMessage;

import java.util.Optional;

public class Result {
  private final CommandResult result;
  private Message original;

  private Result(CommandResult result, Message original) {
    this.result = result;
    this.original = original;
  }

  public static Result Done(CommandResult result,
                            Message original) {
    return new Result(result, original);
  }

  // todo ~> object
  public Optional<SendMessage> makeReply() {
    if (result.isEmpty()) {
      return Optional.empty();
    }

    var msg =  new SendMessage(original.getChatId(), result.getResult())
        .replyToMessageId(original.getMessageId());

    if (result.isHtml()) {
      msg.parseMode(ParseMode.HTML)
         .disableWebPagePreview(true);
    }
    return Optional.of(msg);
  }

}
