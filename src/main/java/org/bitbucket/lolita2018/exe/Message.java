package org.bitbucket.lolita2018.exe;

import com.pengrad.telegrambot.model.Update;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Message {

  private static final Logger logger = LoggerFactory.getLogger(Message.class);

  private final String command;
  private final List<String> args = new ArrayList<>();
  private final long chatId;
  private final int messageId;
  private final UserInfo user;

  // ! ping -> 0, 1, ...
  // ! ping -> _, command, args
  private Message(Update update) {
    var text = Arrays.asList(update.message()
        .text().trim().replaceAll(" +", " ")
        .split(" "));

    chatId = update.message().chat().id();
    messageId = update.message().messageId();
    var userId = update.message().from().id();
    var userName = update.message().from().firstName();

    user = new UserInfo(userId, userName);

    if (text.isEmpty() || text.size() == 1) {
      logger.warn("Failed to process message (is not real command): " + text);
      command = "";
    } else {
      command = text.get(1);
      args.addAll(text.subList(2, text.size()));
    }
  }

  public static Message from(Update update) {
    return new Message(update);
  }

  public String getCommand() {
    return command;
  }

  public long getChatId() {
    return chatId;
  }

  public int getMessageId() {
    return messageId;
  }

  public UserInfo getUser() { return user; }

  public List<String> getArguments() {
    return Collections.unmodifiableList(args);
  }
}
