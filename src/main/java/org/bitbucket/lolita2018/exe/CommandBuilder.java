package org.bitbucket.lolita2018.exe;

import org.bitbucket.lolita2018.plugins.Plugin;

import java.util.ArrayList;

public class CommandBuilder {

  private String command;
  private ArrayList<String> aliases = new ArrayList<>();
  private String helpInfo;
  private Plugin handler;

  private CommandBuilder(String arg) {
    command = arg;
  }

  public static CommandBuilder Command(String name) {
    return new CommandBuilder(name);
  }

  public CommandBuilder alias(String value) {
    if (!aliases.contains(value)) {
      aliases.add(value);
    }
    return this;
  }

  public CommandBuilder help(String text) {
    helpInfo = text;
    return this;
  }

  public CommandBuilder handleWith(Plugin plugin) {
    handler = plugin;
    return this;
  }

  public Command build() {
    if (command == null) {
      throw new IllegalStateException("Command is empty");
    }

    if (handler == null) {
      throw new IllegalStateException("Handler is empty");
    }

    if (helpInfo == null) {
      throw new IllegalStateException("Help is empty");
    }

    return new Command(command, aliases, helpInfo, handler);
  }
}
