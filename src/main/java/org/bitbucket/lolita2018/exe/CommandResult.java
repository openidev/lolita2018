package org.bitbucket.lolita2018.exe;

import java.util.Optional;

// todo @mike you must use inheritance
public class CommandResult {
  public enum Type {
    Html, Text, Video, Empty
  }

  private final String result;
  private final Type type;
  private final Optional<String> caption; // video only

  private CommandResult(String result, Optional<String> caption, Type type) {
    this.result = result;
    this.caption = caption;
    this.type = type;
  }

  public static CommandResult Text(String result) {
    return new CommandResult(result, Optional.empty(), Type.Text);
  }

  public static CommandResult Html(String result) {
    return new CommandResult(result, Optional.empty(), Type.Html);
  }

  public static CommandResult Video(String url, String caption) {
    return new CommandResult(url, Optional.of(caption), Type.Video);
  }

  public static CommandResult Empty() {
    return new CommandResult("", Optional.empty(), Type.Empty);
  }

  public static CommandResult empty = new CommandResult("", Optional.empty(), Type.Empty);

  public static CommandResult emptyText = new CommandResult("", Optional.empty(), Type.Text);


  public boolean isHtml() {
    return type == Type.Html;
  }

  public boolean isEmpty() {
    return type == Type.Empty;
  }

  public boolean isVideo() { return type == Type.Video; }

  public String getResult() {
    return result;
  }
}
