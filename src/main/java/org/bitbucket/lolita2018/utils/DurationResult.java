package org.bitbucket.lolita2018.utils;

import java.time.Duration;
import java.time.LocalDateTime;

public class DurationResult {

  private final long hours;

  private final int minutes;

  public DurationResult(long hours, int minutes) {
    this.hours = hours;
    this.minutes = minutes;
  }

  @Override
  public String toString() {
    var s = new StringBuilder();
    if (hours != 0L) {
      s.append(hours)
          .append("H");
    }

    if (minutes != 0) {
      if (hours != 0) {
        s.append(" ");
      }
      s.append(minutes)
          .append("m");
    }

    return s.toString();
  }

  public long getHours() {
    return hours;
  }

  public int getMinutes() {
    return minutes;
  }

  public static DurationResult Build(LocalDateTime a, LocalDateTime b) {
    var duration = Duration.between(a, b);

    return new DurationResult(duration.toHours(), duration.toMinutesPart());
  }

}
