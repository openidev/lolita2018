package org.bitbucket.lolita2018.utils;

import java.time.LocalDateTime;

public class Value {
  private final String userName;
  private final String voteResult;
  private final long userId;
  private final LocalDateTime voteTime;

  public LocalDateTime getVoteTime() {
    return voteTime;
  }

  public long getUserId() {
    return userId;
  }

  public String getUserName() {
    return userName;
  }

  public String getVoteResult() {
    return voteResult;
  }

  public String getInfo() {
    return "" + userName + " : " + voteResult;
  }

  public Value(long userId, String userName, String voteResult, LocalDateTime voteTime) {
    this.userName = userName;
    this.userId = userId;
    this.voteResult = voteResult;
    this.voteTime = voteTime;
  }
}
