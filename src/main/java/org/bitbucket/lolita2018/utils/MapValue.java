package org.bitbucket.lolita2018.utils;

import java.time.LocalDateTime;

public class MapValue {

  public final String value;
  public final LocalDateTime time;

  private MapValue(String value) {
    this.value = value;
    this.time = LocalDateTime.now();
  }

  public static MapValue Create(String value) {
    return new MapValue(value);
  }

}
