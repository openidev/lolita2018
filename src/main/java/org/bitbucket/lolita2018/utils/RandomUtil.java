package org.bitbucket.lolita2018.utils;

import java.util.*;
import java.util.stream.Collectors;

public class RandomUtil {

  public static String getRandomElement(List<String> xs, Map<Integer, MapValue> map) {
    if (xs.isEmpty()) {
      throw new IllegalArgumentException("Empty collection");
    }
    if (map.isEmpty()) {
      return shead(xs);
    } else {
      var unique = xs.stream().filter(x ->  !map.containsKey(x.hashCode())).collect(Collectors.toList());
      if (unique.isEmpty()) {
        var values = new ArrayList<>(map.values());
        values.sort(Comparator.comparing(a -> a.time));
        return head(values).value;
      }

      return shead(unique);
    }
  }

  private static <T> T shead(List<T> xs) {
    Collections.shuffle(xs);
    return head(xs);
  }

  private static <T> T head(List<T> xs) {
    return xs.get(0);
  }

}
