package org.bitbucket.lolita2018.utils;

import java.util.List;

public class Utils {
    // xs in lowercase
    public static boolean stringContainsFromList(String x, List<String> xs) {
      if (xs.isEmpty()) {
          return false;
        }
      var t = x.toLowerCase();
      return xs.stream().anyMatch(t::contains);
    }

}