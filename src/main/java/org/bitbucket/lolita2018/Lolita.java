package org.bitbucket.lolita2018;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import org.bitbucket.lolita2018.exe.Command;
import org.bitbucket.lolita2018.exe.Message;
import org.bitbucket.lolita2018.exe.Result;
import org.bitbucket.lolita2018.plugins.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static org.bitbucket.lolita2018.exe.CommandBuilder.Command;

public class Lolita {

  private final Logger logger = LoggerFactory.getLogger(Lolita.class);
  private final Configuration configuration;
  private final List<Command> plugins;
  private final TelegramBot bot;
  private final HttpReader client;

  public Lolita(Configuration configuration, HttpReader client) {
    this.configuration = configuration;
    this.client = client;
    plugins = initPlugins();
    this.bot = new TelegramBot.Builder(configuration.getToken()).build();
  }

  public void run() {
    bot.setUpdatesListener((list) -> {
      list.parallelStream()
          .filter(this::isCommand)
          .map((Message::from))
          .flatMap(this::handle)
          .map(r -> r.thenApplyAsync(Result::makeReply))
          .forEach(r -> r.thenAcceptAsync(x -> {
            x.ifPresent(z -> bot.execute(z, MessageCallback.getInstance()));
          }));

      return UpdatesListener.CONFIRMED_UPDATES_ALL;
    });
  }

  private Stream<CompletableFuture<Result>> handle(Message message) {
    return plugins.stream()
        .filter(c -> c.match(message.getCommand()))
        .map(c -> c.handle(message))
        .filter(Optional::isPresent)
        .map(Optional::get)
        //.map(x -> x.theA(x -> x.isEmpty()))
        .map(r -> r.thenApplyAsync(d -> Result.Done(d, message)));
  }

  private List<Command> initPlugins() {

    var cacheProvider = new CacheProvider(25, TimeUnit.HOURS);

    var ping = Command("ping")
        .alias("p")
        .handleWith(new PingPlugin())
        .help("ping-pong")
        .build();

    var version = Command("version")
        .alias("v")
        .handleWith(new VersionPlugin())
        .help("display current Lolita Version")
        .build();

    var lor = Command("lor")
        .alias("l")
        .alias("лор")
        .help("Lor news")
        .handleWith(new LorPlugin(client))
        .build();

    var elementy = Command("elementy")
        .alias("el")
        .alias("эл")
        .help("Новости с элементов")
        .handleWith(new ElementyPlugin(client))
        .build();

    var habr = Command("habr")
        .alias("habr")
        .alias("хабр")
        .alias("х")
        .alias("x")
        .help("Новости с хабра")
        .handleWith(new HabrPlugin(client))
        .build();

    var bash = Command("bash")
        .alias("b")
        .alias("б")
        .alias("bash")
        .alias("баш")
        .help("bash |> random")
        .handleWith(new BashPlugin(client))
        .build();

    var one = Command("1")
        .help("1")
        .handleWith(new OnePlugin())
        .build();

    var hn = Command("hn")
        .help("хн")
        .handleWith(new HackerNewsPlugin(client))
        .help("новини z hacker.news")
        .build();


    var hogwarts = Command("hog")
        .alias("!")
        .help("Расределение")
        .handleWith(new HogwartsPlugin(cacheProvider))
        .build();

    var makaba = Command("любимый тред")
        .alias("lt")
        .alias("лт")
        .alias("~")
        .help("любимый тред")
        .handleWith(new MakabaPlugin(client))
        .build();

    var anekdot = Command("анекдот")
        .alias("a")
        .alias("а")
        .alias("f")
        .alias("ан")
        .alias("an")
        .help("анек")
        .handleWith(new AnekdotPlugin(client))
        .build();

    var anekdotNew = Command("анекдот")
        .alias("a")
        .alias("а")
        .alias("f")
        .alias("ан")
        .alias("an")
        .help("анек")
        .handleWith(new Anekdot2Plugin(client))
        .build();

    var distributionResults = Command("результаты")
        .alias("%")
        .alias("рез")
        .help("рэзультаты размеркаваньня")
        .handleWith(new DistributionResultsPlugins(cacheProvider))
        .build();

    var tmpHelp = Command("help")
        .alias("h")
        .alias("?")
        .handleWith(Plugin.Empty)
        .help("return help info")
        .build();

    var help = Command("help").alias("h")
        .alias("?")
        .handleWith(new HelpPlugin(ping, version, lor, tmpHelp,
            elementy, habr, bash, one, hn, makaba, anekdotNew, hogwarts, distributionResults))
        .help("return help info")
        .build();

    return List.of(ping, help, version, lor, elementy,
        habr, bash, one, hn,  makaba, anekdotNew, hogwarts, distributionResults);
  }

  private boolean isCommand(Update update) {
    if (update == null) {
      return false;
    }

    if (update.message() == null) {
      return false;
    }

    var text = update.message().text();

    return text != null && text.trim().startsWith(configuration.getCommand());
  }
}
