package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.jsoup.nodes.TextNode;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class BashPlugin extends HttpPlugin {

  private static final String url = "http://bash.im/random";
  private static final String selector = ".text";

  public BashPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAsDoc(url).thenApplyAsync(doc -> {
      var xs = doc.select(selector).stream()
          .map(x -> x.textNodes().stream().map(TextNode::text).collect(line))
          .collect(Collectors.toList());
      return CommandResult.Text(getRandomElement(xs));
    });
  }
}
