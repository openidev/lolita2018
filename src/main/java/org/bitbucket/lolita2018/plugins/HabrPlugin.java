package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class HabrPlugin extends HttpPlugin {

  private final String url = "https://habrahabr.ru/";

  public HabrPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAsDoc(url).thenApplyAsync(doc ->
      makeSublist(doc.select(".post")).stream().map(post -> {
        var t = post.select("h2 a.post__title_link");
        var c = post.select("span.post-stats__comments-count").first();
        int count = 0;
        if (c != null) {
          count = toInt(c.text());
        }
        var url = t.attr("href");
        return a1(t.text(), url) + " [" + count + "]";
      }).collect(line)
    ).thenApplyAsync(CommandResult::Html);
  }

  private int toInt(String text) {
    try {
      return Integer.parseInt(text);
    } catch (Exception ex) {
      return 0;
    }
  }
}
