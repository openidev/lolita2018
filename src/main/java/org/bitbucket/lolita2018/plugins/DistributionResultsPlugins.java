package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.bitbucket.lolita2018.utils.Value;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class DistributionResultsPlugins extends Plugin {

  private final CacheProvider provider;

  public DistributionResultsPlugins(CacheProvider provider) {
    this.provider = provider;
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo user) {
    var s = new StringBuilder();

    var map = provider.getResults()
        .stream()
        .collect(Collectors.groupingBy(Value::getVoteResult));

    if (map.isEmpty()) {
      return cf("try <! !>");
    }

    for(Map.Entry<String, List<Value>> entry : map.entrySet()) {
      var users = entry.getValue().stream().map(Value::getUserName).collect(Collectors.joining(comma));
      s.append(entry.getKey())
          .append(": ")
          .append(users)
          .append(delimiter);
    }


    return cf(s.toString());
  }

  private static String comma = ", ";
}
