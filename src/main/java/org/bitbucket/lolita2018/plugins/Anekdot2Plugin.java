package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class Anekdot2Plugin extends HttpPlugin {

  private static final String url = "http://anekdotovstreet.com/";

  private static final String selector = ".anekdot-text > p";

  private static final List<String> urls = List.of(
      "nacionalnosti/negry/",
      "nacionalnosti/chukcha/",
      "nacionalnosti/anglichane/",
      "nacionalnosti/dagestancy/",
      "svegie-anekdoty/",
      "vovochka/",
      "blondinki/",
      "armiya/voenkomat/",
      "armiya/desantniki-den-vdv/",
      "armiya/praporschik/",
      "armiya/soldat/",
      "armiya/",
      "novye-russkie/",
      "sverhestestvennoe/",
      "religiya/",
      "religiya/monashki/",
      "shkola/",
      "rabota-professii/",
      "poshlye/",
      "politicheskie/"
  );

  private static int length = urls.size();

  private String getRandomCategory() {
    var index = ThreadLocalRandom.current().nextInt(0, length);
    return url + urls.get(index);
  }

  public Anekdot2Plugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    var url = getRandomCategory();
    logger.debug("Get from: " + url);

    return client.readAndSelect(url, selector)
        .thenApplyAsync(xs -> CommandResult.Text(getRandomElement(xs)));
  }
}
