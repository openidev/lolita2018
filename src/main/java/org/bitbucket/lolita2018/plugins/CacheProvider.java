package org.bitbucket.lolita2018.plugins;

import net.jodah.expiringmap.ExpiringMap;
import org.bitbucket.lolita2018.utils.Value;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CacheProvider {

  private final Map<Long, Value> map;
  private final int expiration;
  private final TimeUnit unit;

  public CacheProvider(int expiration, TimeUnit unit) {
    this.unit = unit;
    this.expiration = expiration;
    map =  ExpiringMap.builder()
        .expiration(expiration, unit)
        .build();
  }

  public Value get(long key) {
    return map.get(key);
  }

  public void put(long key, Value value) {
    map.put(key, value);
  }

  public ArrayList<Value> getResults() {
    return new ArrayList<>(map.values());
  }

  public TimeUnit getCurrentUnit() {
    return unit;
  }

  public int getExpiration() {
    return expiration;
  }

}
