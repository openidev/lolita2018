package org.bitbucket.lolita2018.plugins;

import net.jodah.expiringmap.ExpiringMap;
import org.bitbucket.lolita2018.utils.MapValue;
import org.bitbucket.lolita2018.utils.RandomUtil;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

abstract class HttpPlugin extends Plugin {

  protected HttpReader client;
  protected Map<Integer, MapValue> map;

  HttpPlugin(HttpReader client) {
    this.client = client;
    this.map =  ExpiringMap.builder()
        .expiration(24, TimeUnit.HOURS)
        .build();
  }

  protected String getRandomElement(List<String> xs) {
    var result = RandomUtil.getRandomElement(xs, map);
    map.putIfAbsent(result.hashCode(), MapValue.Create(result));
    return result;
  }



}
