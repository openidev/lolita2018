package org.bitbucket.lolita2018.plugins;

import com.google.gson.Gson;
import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.bitbucket.lolita2018.utils.Utils;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class MakabaPlugin extends HttpPlugin {

  private static String url = "https://2" + "ch" + ".hk";

  private static List<String> lt = List.of("засмеял", "проигр");

  private static Gson gson = new Gson();

  public MakabaPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.read(url + "/b/threads.json").thenApplyAsync(body ->
      gson.fromJson(body, MakabaThreadResult.class)
    )
    .thenApplyAsync(x -> x.threads.stream().filter(y -> Utils.stringContainsFromList(y.comment, lt)))
    .thenApplyAsync(xs -> {
      var list = xs.collect(Collectors.toList());
      if (list.isEmpty()) {
        return CommandResult.Text(":(");
      } else if (list.size() == 1) {
        var head = list.get(0);
        return CommandResult.Html(a(head.subject, url + "/b/res" + head.num + ".html"));
      } else {
        var lst = list.stream()
            .map(x -> {
              var head = x.subject;
              var href = url + "/b/res/" + x.num + ".html";
              return a1(head, href);
            }).collect(line);
        return CommandResult.Html(lst);
      }
    });
  }



  private static class MakabaThreadResult {
    public String board;
    public List<MakaraThreadLight> threads;
  }

  private static class MakaraThreadLight {
    public String comment;
    public long lasthit;
    public long num;
    public int post_count;
    public double score;
    public String subject;
    public long timestamp;
    public int views;
  }


}
