package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public abstract class Plugin {
  protected int begin = 0;
  protected int end = 6;

  protected static final Logger logger = LoggerFactory.getLogger(Plugin.class);

  protected static CompletableFuture<CommandResult> cf(String value) {
    return cf(CommandResult.Text(value));
  }

  protected static CompletableFuture<CommandResult> cf(CommandResult value) {
    return CompletableFuture.completedFuture(value);
  }

  protected static String a(String title, String url) {
    return "<a href='" + url + "'>" + title + "</a>";
  }

  protected static String a1(String title, String url) {
    return title + " " + a("link", url);
  }

  // Future.done
  // todo pass full message
  public abstract CompletableFuture<CommandResult> run(List<String> arg, UserInfo user);

  public static Plugin Empty = new Plugin() {
    @Override
    public CompletableFuture<CommandResult> run(List<String> arg, UserInfo user) {
      return cf(CommandResult.emptyText);
    }
  };

  protected <T> List<T> makeSublist(List<T> xs) {
    if (xs.isEmpty()) {
      return xs;
    }

    if (xs.size() >= end) {
      return xs.subList(begin, end);
    }

    return xs;
  }

  protected static String delimiter = "\n";
  protected static Collector<CharSequence, ?, String> line = Collectors.joining(delimiter);
}
