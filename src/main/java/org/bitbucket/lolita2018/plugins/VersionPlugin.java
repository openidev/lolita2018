package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class VersionPlugin extends Plugin {

  private static final CommandResult version = CommandResult.Text("v4.0.0");

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return cf(version);
  }
}
