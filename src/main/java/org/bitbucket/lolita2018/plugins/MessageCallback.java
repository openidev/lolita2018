package org.bitbucket.lolita2018.plugins;

import com.pengrad.telegrambot.Callback;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class MessageCallback implements Callback<SendMessage, SendResponse> {

  private static final Logger logger = LoggerFactory.getLogger(MessageCallback.class);

  private static MessageCallback instance = new MessageCallback();

  public static MessageCallback getInstance() {
    return instance;
  }

  @Override
  public void onResponse(SendMessage a, SendResponse b) {

  }

  @Override
  public void onFailure(SendMessage a, IOException e) {

    logger.warn("Failed to send request -> " + e.getMessage());
  }
}
