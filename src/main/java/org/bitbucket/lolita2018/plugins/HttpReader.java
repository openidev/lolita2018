package org.bitbucket.lolita2018.plugins;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class HttpReader {

  private static final Logger logger = LoggerFactory.getLogger(HttpReader.class);

  private final static long cT = 10L;
  private final static long rT = 45L;
  private final static String ua = "Mozilla/5.0 (X11; Russian Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Safari/537.36";
  private final static String hUa = "User-Agent";

  private final HttpClient client;

  private final static TrustManager[] certs = new TrustManager[] {
      new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null; }
        public void checkClientTrusted(X509Certificate[] certs, String authType) { }
        public void checkServerTrusted(X509Certificate[] certs, String authType) { }
      }
  };



  private HttpReader() {
    SSLContext sslContext = null;
    try {
      sslContext = SSLContext.getInstance("SSL");

      sslContext.init(null, certs, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) { return true; }
      };
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    } catch (Exception ex) {
      logger.warn("Failed to initialize SSL: ", ex);
    }

    var tmp = HttpClient.newBuilder()
        .connectTimeout(Duration.ofSeconds(cT))
        .followRedirects(HttpClient.Redirect.ALWAYS)
        .version(HttpClient.Version.HTTP_1_1);

    if (sslContext != null) {
      tmp.sslContext(sslContext);
    }

    client = tmp.build();
  }

  public static HttpReader build() {
    return new HttpReader();
  }

  public CompletableFuture<String> read(String url) {
    var request = HttpRequest.newBuilder()
        .GET()
        .header(hUa, ua)
        .timeout(Duration.ofSeconds(rT))
        .uri(URI.create(url))
        .build();

    return client
        .sendAsync(request, HttpResponse.BodyHandlers.ofString())
        .handleAsync((t, ex) -> {
          if (ex != null) {
            logger.warn("Exception on request: " + url, ex.getCause());
            return null;
          }
          logger.info("Result for " + url + " --- " + t.statusCode());
          return t.body();
        });
  }

  public CompletableFuture<Document> readAsDoc(String url) {
    return read(url).thenApplyAsync(Jsoup::parse);
  }

  public CompletableFuture<List<String>> readAndSelect(String url, String selector) {
    return readAsDoc(url).thenApplyAsync(doc -> doc
        .select(selector)
        .stream()
        .map(Element::text)
        .collect(Collectors.toList()));
  }

}
