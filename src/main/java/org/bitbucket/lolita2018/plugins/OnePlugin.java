package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class OnePlugin extends Plugin {

  private static final CompletableFuture<CommandResult> one =
      cf(CommandResult.Text("1"));

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return one;
  }
}
