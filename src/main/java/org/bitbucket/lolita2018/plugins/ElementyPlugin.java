package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.jsoup.nodes.TextNode;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class ElementyPlugin extends HttpPlugin {

  private static final String url = "http://elementy.ru/rss/news";

  public ElementyPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAsDoc(url).thenApplyAsync(doc -> {
      var result = makeSublist(doc.select("item")).stream().map(item -> {
        var title = item.select("title").text();
        var link = item.textNodes().stream().filter(x -> x.text().startsWith("http:"))
            .findFirst().map(TextNode::text).orElse("");
        return a1(title, link);
      }).collect(line);
      return CommandResult.Html(result);
    });
  }
}
