package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class AnekdotPlugin extends HttpPlugin {

  private final static String url = "https://www.anekdot.ru/last/anekdot/";
  private final static String selector = ".topicbox > .text";

  public AnekdotPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAndSelect(url, selector)
        .thenApplyAsync(xs -> CommandResult.Text(getRandomElement(xs)));
  }
}
