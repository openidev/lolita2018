package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.bitbucket.lolita2018.utils.DurationResult;
import org.bitbucket.lolita2018.utils.Value;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;

public class HogwartsPlugin extends Plugin {

  private static List<String> houses = List.of(
      "Слизерин",
      "Когтевран",
      "Пуффендуй",
      "Гриффиндор"
  );

  private static int length = houses.size();

  private final CacheProvider provider;

  public HogwartsPlugin(CacheProvider provider) {
    this.provider = provider;
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    var userId = userInfo.getUserId();

    var now = LocalDateTime.now();

    var r = provider.get(userId);

    if (r != null) {
      var n = now.plus(provider.getExpiration(), provider.getCurrentUnit().toChronoUnit());
      var f = friendly(r.getVoteTime(), n);

      logger.debug("User " + userId + " already voted, next: " + f);

      return cf("Next: " + f);
    }

    var index = ThreadLocalRandom.current().nextInt(0, length);
    var house = houses.get(index);

    var value = new Value(userId, userInfo.getName(), house, now);

    provider.put(userId, value);

    return cf(house);
  }

  private static String friendly(LocalDateTime now, LocalDateTime next) {
    return DurationResult.Build(now, next).toString();
  }
}
