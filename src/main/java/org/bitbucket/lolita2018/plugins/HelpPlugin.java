package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.Command;
import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class HelpPlugin extends Plugin {

  private final CommandResult help;

  public HelpPlugin(Command... available) {
    help = CommandResult.Text(
        Arrays.stream(available)
        .map(Command::help)
        .collect(line)
    );
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return cf(help);
  }
}
