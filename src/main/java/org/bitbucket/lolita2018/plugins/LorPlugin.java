package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class LorPlugin extends HttpPlugin {

  private final String url = "https://www.linux.org.ru";

  public LorPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAsDoc(url).thenApplyAsync(doc ->
      makeSublist(doc.select("article")).stream().map(e -> {
        var hs = e.select("h2 > a");
        Element el;
        if (hs == null || hs.size() == 0) {
          el = e.select("a").first();
        } else {
          el = hs.first();
        }
        var txt = el.textNodes().stream()
            .map(TextNode::toString).collect(Collectors.joining(" "));
        var lnk = url + el.attr("href");
        return a1(txt, lnk);
      }).collect(line)
    ).thenApplyAsync(CommandResult::Html);
  }
}
