package org.bitbucket.lolita2018.plugins;

import org.bitbucket.lolita2018.exe.CommandResult;
import org.bitbucket.lolita2018.exe.UserInfo;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public class HackerNewsPlugin extends HttpPlugin {

  private static final String url = "https://news.ycombinator.com/";

  public HackerNewsPlugin(HttpReader client) {
    super(client);
  }

  @Override
  public CompletableFuture<CommandResult> run(List<String> arg, UserInfo userInfo) {
    return client.readAsDoc(url).thenApplyAsync(doc ->
      makeSublist(doc.select("tr.athing > td.title > a"))
          .stream().map(element -> {
            var h = element.text();
            var url = element.attr("href");
            return a1(h, url);
      }).collect(line)
    ).thenApplyAsync(CommandResult::Html);
  }
}
