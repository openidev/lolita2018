package org.bitbucket;

import com.typesafe.config.ConfigFactory;
import org.bitbucket.lolita2018.Configuration;
import org.bitbucket.lolita2018.Lolita;
import org.bitbucket.lolita2018.plugins.HttpReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.bitbucket.lolita2018.Configuration.Fields;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
      var c = ConfigFactory.load();
      var configuration = new Configuration(
        c.getString(Fields.fCommand),
        c.getString(Fields.fToken),
        c.getLong(Fields.fChatId),
        c.getLongList(Fields.fChats)
      );

      var client = HttpReader.build();

      var lolita = new Lolita(configuration, client);
      lolita.run();

      logger.info("Lolita is up and running");
    }
}
